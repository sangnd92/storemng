from flask import Blueprint, request, make_response, jsonify
from flask.views import MethodView

from server import bcrypt, db
from server.models import Account, BlacklistToken

auth_blueprint = Blueprint('auth', __name__)


class RegisterApi(MethodView):
    """
    User Registration Resource
    """

    def post(self):
        # get the post data
        post_data = request.get_json()
        user = Account.query.filter_by(email=post_data.get('username')).first()
        if not user:
            pass
        else:
            responseObject = {
                'status': 'fail',
                'message': 'User already exists. Please Log in.'
            }
            return make_response(jsonify(responseObject)), 202


class LoginApi(MethodView):
    def post(self):
        pass


class LogoutApi(MethodView):
    def post(self):
        pass


class ChangePasswordApi(MethodView):
    def post(self):
        pass


class ForgotPasswordApi(MethodView):
    def post(self):
        pass


class ActiveUserApi(MethodView):
    def get(self):
        pass


# define the api resource
registion_view = RegisterApi.as_view('register_api')
login_view = LoginApi.as_view('login_api')
logout_view = LogoutApi.as_view('logout_view')
change_password_view = ChangePasswordApi.as_view('change_password_view')
forgot_password_view = ForgotPasswordApi.as_view('forgot_password_view')
active_user_view = ActiveUserApi.as_view('active_user_view')

# add rules for api endpoints
auth_blueprint.add_url_rule(
    'auth/register',
    view_func=registion_view,
    methods=['POST']
)
auth_blueprint.add_url_rule(
    'auth/login',
    view_func=login_view,
    methods=['POST']
)
auth_blueprint.add_url_rule(
    'auth/logout',
    view_func=logout_view,
    methods=['POST']
)
auth_blueprint.add_url_rule(
    'auth/change_password',
    view_func=change_password_view,
    methods=['POST']
)
auth_blueprint.add_url_rule(
    'auth/forgot_password',
    view_func=forgot_password_view,
    methods=['POST']
)
auth_blueprint.add_url_rule(
    'auth/active_user',
    view_func=active_user_view,
    methods=['GET']
)
