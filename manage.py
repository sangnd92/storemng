from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from server import db, app, models

migrate = Migrate(app, db)
manager = Manager(app)

# migration
manager.add_command('db', MigrateCommand)


@manager.command
def create_db():
    """Creates the database tables"""
    db.create_all()


@manager.command
def drop_db():
    """Drop the db tables"""
    db.drop_all()


if __name__ == '__main__':
    manager.run()
